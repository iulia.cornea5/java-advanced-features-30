import zoo.animal.Animal;
import zoo.animal.AnimalService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class AnimalServiceTest {

    AnimalService service = new AnimalService("Test");

    @Test
    public void animalCreateTest() {

        // Given
       AnimalService  service2;
       service2 = new AnimalService("Test2");

        // When
       Animal actualResult = service.createAnimal("George", "Tigru", 10);

        // Then
        Assertions.assertEquals("Geroge", actualResult.getName());
        Assertions.assertEquals("Tigru", actualResult.getSpecies());
        Assertions.assertEquals(10, actualResult.getAge());

        Assertions.assertEquals("Name: Geroge Species: Tigru Age: 10", actualResult.toString());

        System.out.println(Arrays.deepToString(AnimalService.animals));
    }

    @Test
    @Disabled
    public void animalCreateTestAssert4J() {
        // Given
        String animalName = "Geroge";
        String animalSpecies = "Tigru";
        int animalAge = 3;

        // When
        Animal animal = service.createAnimal(animalName, animalSpecies, animalAge);

        // Then
        Assertions.assertEquals("Geroge", animal.getName());
        Assertions.assertEquals(animalSpecies, animal.getSpecies());
        Assertions.assertEquals(animalAge, animal.getAge());

        Assertions.assertEquals(1, AnimalService.contor);
        Assertions.assertEquals(animalName, AnimalService.animals[0].getName());
        Assertions.assertEquals(animalSpecies, AnimalService.animals[0].getSpecies());
        Assertions.assertEquals(animalAge, AnimalService.animals[0].getAge());

        System.out.println(AnimalService.animals.toString());
    }
}
