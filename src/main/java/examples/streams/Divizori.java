package examples.streams;

import java.util.*;
import java.util.stream.Collectors;

public class Divizori {

    public static void main(String[] args) {

        /*
filter
map

peek
distinct
sorted
limit
reduce
         */

        List<Integer> numere = Arrays.asList(1, null, 21, 43, null, 32, 234, 35, 5, 3, null, 2, 43, 32, 523, null, 2, 43, 32, 523);

        boolean rezultat = numere.stream()
                .filter((n) -> {
                    return n != null;
                })
                .map(n -> n * (-1))
                .distinct()
                .sorted(Comparator.naturalOrder())
                .limit(5)
                .peek(System.out::println)
                .allMatch(n -> {
                    return n > -650;
                });
        System.out.println("Rezultat " + rezultat);

        long suma = numere.stream()
                .filter(n -> n != null)
                .reduce(0, (rezultatAnterior, elementCurent) -> rezultatAnterior + elementCurent);
        System.out.println("Suma " + suma);


        List<String> cuvinte = List.of("a", "b", "c", "d", "e");
        String concatenare = cuvinte.stream().reduce("", (rez, s) -> {return rez + s;});
        System.out.println(concatenare);
    }


    public static void afiseazaTriplu(int nr) {
        System.out.print(" " + nr * 3);
    }


    public static void afiseazaDivizori(int i) {
        for (int d = 1; d <= i; d++) {
            if (i % d == 0) {
                System.out.print(d);
            }
        }
        System.out.println("");
    }

    public static ArrayList<Integer> getToOneHundred() {
        ArrayList<Integer> tenThousand = new ArrayList<>();
        for (int i = 1; i <= 10000; i++) {
            tenThousand.add(i);
        }
        return tenThousand;
    }
}
























