package examples.collections;

import examples.interfacesAndClasses.plants.*;

import java.io.ObjectStreamException;
import java.util.*;

public class Application {

    public static void main(String[] args) {

        listExamples();
        setExamples();
        mapExamples();


        findGeatest(Collections.EMPTY_LIST, null);
        findGeatest(Collections.singletonList("ana"), null);
    }

    public static void findGeatest(List list, Comparator comparator) {

    }

    private static void mapExamples() {
        Map<Integer, String> champions = new HashMap<>();
        champions.put(1, "ana");
        champions.put(3, "maria");
        champions.put(2, "ion");
        champions.put(4, "george");
        champions.put(5, "vlad");
        champions.put(5, "zzzzzzzzzzzzz");

        System.out.println(champions.get(3));
        champions.containsValue("george");
        for(String nume: champions.values()) {
            System.out.println(nume);
        }

        Comparator<String> ordineInversa = Comparator.reverseOrder();
        Comparator<String> oridneaMea = new Comparator<String>() {
            // daca o1 < o2 => ceva negativ
            // daca o1 == o1 => 0 (zero)
            // daca o1 > o2 => ceva pozitiv
            @Override
            public int compare(String o1, String o2) {
                if(o1.length() < o2.length()) {
                    return -1;
                } else if(o1.length() == o2.length()) {
                    return 0;
                } else
                    return 1;
            }
        };

        // tree map => ordoneaza obiectele in functie de ordinea naturala a cheilor KEY
        Map<String, Plant> treeMapPlants = new TreeMap<>(oridneaMea);
        treeMapPlants.put("trandafir", new Rose("Trandafir rosu", 10, 100));
        treeMapPlants.put("afin", new Blueberry("Afin de ghiveci", 10, 100));
        treeMapPlants.put("iedera", new Ivy("Iedera de gradina", 10, 100));
        treeMapPlants.put("ciuperca", new Mushroom("Palaria sarpelui", 10, 100));

        for(Plant p : treeMapPlants.values()) {
            System.out.println(p.getName());
        }

        // linked hash map => pastreaza ordinea de inserare
        Map<String, Plant> LinkedHashMapPlants = new LinkedHashMap<>();
        LinkedHashMapPlants.put("trandafir", new Rose("Trandafir rosu", 10, 100));
        LinkedHashMapPlants.put("afin", new Blueberry("Afin de ghiveci", 10, 100));
        LinkedHashMapPlants.put("iedera", new Ivy("Iedera de gradina", 10, 100));
        LinkedHashMapPlants.put("ciuperca", new Mushroom("Palaria sarpelui", 10, 100));

        for(Plant p : LinkedHashMapPlants.values()) {
            System.out.println(p.getName());
        }

    }

    private static void setExamples() {
        System.out.println("------------------------------SET-------------------------");
        System.out.println("HashSet - ordine aleatoare - nici cum le-am pus noi, nici in oridine \"naturala\"");
        Set<Integer> hashSetIneger = new HashSet<Integer>();
        hashSetIneger.add(0);
        hashSetIneger.add(1);
        hashSetIneger.add(2);
        hashSetIneger.add(3);
        hashSetIneger.add(8);
        hashSetIneger.add(2);
        hashSetIneger.add(1);
        hashSetIneger.add(11);
        hashSetIneger.add(666);
        hashSetIneger.remove(8);

        for(Integer i : hashSetIneger) {
            System.out.println(i);
        }

        System.out.println("TreeSet - ordine naturala - nu neaparat cum le-am pus noi, ci in oridine \"naturala\"");
        Set<String> treeSetStrings = new TreeSet<>();
        treeSetStrings.add("Bula");
        treeSetStrings.add("Strula");
        treeSetStrings.add("Alinuta");

        for (String s: treeSetStrings) {
            System.out.println(s);
        }

        System.out.println("LinkedHashSet - ordinea in care au fost inserate");
        Set<String> linkedHashSet = new LinkedHashSet<String>();
        linkedHashSet.add("Zorica");
        linkedHashSet.add("Ana");
        linkedHashSet.add("Marcel");

        for (String s : linkedHashSet) {
            System.out.println(s);
        }

        if(linkedHashSet.contains("Ana")) {
            System.out.println("Are ana");
        } else {
            System.out.println("N-are ana");
        }

    }

    private static void listExamples() {
        System.out.println("------------------------------LIST-------------------------");
        System.out.println("ArrayList - Faster for .get(i), slower for .add(i, x)");
        List<Element> elements = new ArrayList<Element>();
        elements.add(new Element("Mihai"));
        elements.add(new Element("Ion"));
        elements.add(new Element("Anreea"));
        elements.add(new Element("Vasile"));
        elements.add(new Element("George"));
        elements.add(new Element("Ioana"));
        elements.add(2, new Element("Mihaela"));

        List<Element> linkedElements = new LinkedList<>(elements);

//        for(int i = 0; i< elements.size(); i++) {
//            System.out.println(elements.get(i).numar);
//        }
//
        for(Element e : elements) {
            System.out.println(e.nume);
        }

        System.out.println("LinkedList - Slower for .get(i), faster for .add(i, x)");
        List<Integer> listIntegers = new LinkedList<>();
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(3);
        listIntegers.add(2);
        listIntegers.add(1);
        listIntegers.add(0,10);

        for(Integer i : listIntegers) {
            System.out.println(i);
        }


    }
}
