package examples.files;

import java.io.*;

public class FileExercises {


    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;

        try {
            bufferedReader = new BufferedReader(new FileReader("C:\\\\_\\\\SDA\\\\JavaAdvancedFeatures\\\\src\\\\main\\\\resources\\\\test.txt"));
            bufferedWriter = new BufferedWriter(new FileWriter("C:\\\\_\\\\SDA\\\\JavaAdvancedFeatures\\\\src\\\\main\\\\resources\\\\output_test.txt"));


            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                bufferedWriter.write(line + "\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
        }
    }

    public static void fileReaders() throws IOException {

        FileReader reader = null;
        FileWriter writer = null;

        try {
            reader = new FileReader("C:\\\\_\\\\SDA\\\\JavaAdvancedFeatures\\\\src\\\\main\\\\resources\\\\test.txt");
            writer = new FileWriter("C:\\\\_\\\\SDA\\\\JavaAdvancedFeatures\\\\src\\\\main\\\\resources\\\\output_test.txt");

            int nextChat;
            while ((nextChat = reader.read()) != -1) {
                System.out.println(nextChat);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
}
