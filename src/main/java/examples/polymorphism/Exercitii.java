package examples.polymorphism;

import examples.polymorphism.forme.Forma2D;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercitii {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String raspuns;

        ArrayList<Forma2D> forme = new ArrayList<Forma2D>();
        do {
            System.out.println("Alegeți forma pe care doriți să o creați\n" +
                    "1. Cerc\n" +
                    "2. Pătrat\n" +
                    "3. Triunghi");

            int nrFormei = keyboard.nextInt();
            keyboard.nextLine();

            ConstructorForme2D constructor = new ConstructorForme2D();

            Forma2D formaNoastra;
            formaNoastra = constructor.creazaForma2D(nrFormei);

            forme.add(formaNoastra);
//            formaNoastra.afiseazaForma();
            System.out.println("Mai vreți să creați o formă? Y/N");
            raspuns = keyboard.nextLine();
        } while (raspuns.equals("Y"));

        int lungimeForme = forme.size();
        for (int i = 0; i < lungimeForme; i++) {
            forme.get(i).afiseazaForma();
        }
        // Suma perimetrelor este:
        // Perimetrul mediu este:
        // Suma suprafetelor este:
        // Suprafata medie este:
    }
}