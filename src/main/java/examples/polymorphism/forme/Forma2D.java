package examples.polymorphism.forme;

import examples.polymorphism.forme.trasaturi.Bordura;

public abstract class Forma2D {
    String culoare = "Negru";
    Bordura bordura = Bordura.SOLID;

    public Forma2D() {

    }

    public abstract double getPerimetru();
    public abstract double getAria();

    public void afiseazaForma() {
        System.out.println("Aceasta este o formă" + getCuloare() + " 2D cu perimetrul de " + getPerimetru());
    }

    public void setCuloare(String culoareNoua) {
        this.culoare = culoareNoua;
    }

    public String getCuloare() {
        return culoare;
    }

    public Bordura getBordura() {
        return bordura;
    }

    public void setBordura(Bordura bordura) {
        this.bordura = bordura;
    }
}
