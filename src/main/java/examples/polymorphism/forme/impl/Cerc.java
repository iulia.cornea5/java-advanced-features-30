package examples.polymorphism.forme.impl;

import examples.polymorphism.forme.Forma2D;
import examples.polymorphism.forme.trasaturi.Bordura;

public class Cerc extends Forma2D {
    private int raza;

    public Cerc(int raza) {
//        super();
        this.raza = raza;
    }

    public double getPerimetru() {
        return  2 * 3.14 * this.raza;
    }

    @Override
    public double getAria() {
        return 3.14 * raza * raza;
    }

    @Override
    public void afiseazaForma() {
        System.out.println("Acesta este un Cerc " + getCuloare() +
                " cu bordura " + getBordura() +
                " cu perimetrul de " + getPerimetru() +
                " și aria de " + getAria());
    }

}
