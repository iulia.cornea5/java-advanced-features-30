package examples.polymorphism.forme.impl;

import examples.polymorphism.forme.Forma2D;

public class Triunghi extends Forma2D {
    private int baza;
    private int inaltimea;
    private int l1;
    private int l2;

    public Triunghi(int l1, int l2, int baza, int inaltimea) {
        this.l1 = l1;
        this.l2 = l2;
        this.baza = baza;
        this.inaltimea = inaltimea;
    }

    public double getPerimetru() {
        if (l1 == 0 || l2 == 0 || baza == 0) {
            ArithmeticException exception =
                    new ArithmeticException("Triunghuil nu are toate laturile initializate ");
            throw exception;
        }
        return l1 + l2 + baza;
    }

    @Override
    public double getAria() {
        return baza * inaltimea / 2;
    }


    @Override
    public void afiseazaForma() {
        System.out.println("Acesta este un Triunghi " + getCuloare() +
                " cu bordura " + getBordura() +
                " cu perimetrul de " + getPerimetru() +
                " și aria de " + getAria());
    }

}
