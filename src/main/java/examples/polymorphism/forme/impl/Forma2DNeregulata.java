package examples.polymorphism.forme.impl;

import examples.polymorphism.forme.Forma2D;

public class Forma2DNeregulata extends Forma2D {
    private double perimetru;
    private double aria;

    public Forma2DNeregulata(double perimetru, double aria) {
        this.perimetru = perimetru;
        this.aria = aria;
    }


    @Override
    public double getPerimetru() {
        return perimetru;
    }

    @Override
    public double getAria() {
        return aria;
    }


}
