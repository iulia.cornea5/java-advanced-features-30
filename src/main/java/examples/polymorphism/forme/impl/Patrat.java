package examples.polymorphism.forme.impl;

import examples.polymorphism.forme.Forma2D;

public class Patrat extends Forma2D {
    private int latura;

    public Patrat(int latura) {
        this.latura = latura;
    }

    public double getPerimetru() {
        return 4 * latura;
    }

    public double getAria() {
        return latura * latura;
    }


    @Override
    public void afiseazaForma() {
        System.out.println("Acesta este un Pătrat " + getCuloare() +
                " cu bordura " + getBordura() +
                " cu perimetrul de " + getPerimetru() +
                " și aria de " + getAria());
    }


}
