package examples.polymorphism;

import examples.polymorphism.forme.Forma2D;
import examples.polymorphism.forme.impl.Cerc;
import examples.polymorphism.forme.impl.Patrat;
import examples.polymorphism.forme.impl.Triunghi;
import examples.polymorphism.forme.trasaturi.Bordura;

import java.util.Scanner;

public class ConstructorForme2D {
    Scanner keyboard = new Scanner(System.in);
//
//    public ConstructorForme2D() {
//
//    }

    public Forma2D creazaForma2D(int nrFormei) {
        switch (nrFormei) {
            case 1 :
                return creazaCerc();
            case 2:
                return creazaPatrat();
            case 3:
                return creazaTriunghi();
        }
        return null;
    }

    public Cerc creazaCerc() {
        System.out.println("Dati raza cercului");
        int raza = keyboard.nextInt(); // '\n'
        keyboard.nextLine();
        Cerc c = new Cerc(raza);
        seteazaTrasaturi(c);
        return c;
    }

    public Patrat creazaPatrat() {
        System.out.println("Dati latura patratului");
        int latura = keyboard.nextInt();
        keyboard.nextLine();
        Patrat p = new Patrat(latura);
        seteazaTrasaturi(p);
        return p;
    }

    public Triunghi creazaTriunghi() {
        System.out.println("Dati laturile triunghiului " +
                "si inaltimea fata de ultima latura");
        int l1 = keyboard.nextInt();
        int l2 = keyboard.nextInt();
        int l3 = keyboard.nextInt();
        int b = keyboard.nextInt();
        keyboard.nextLine();
        Triunghi t = new Triunghi(l1, l2, l3, b);
        seteazaTrasaturi(t);
        return t;
    }

    private void seteazaTrasaturi(Forma2D forma) {
        System.out.println("Dati bordura");
        String bordura = keyboard.nextLine();
        forma.setBordura(Bordura.valueOf(bordura));
        System.out.println("Dati culoarea");
        String culoare = keyboard.nextLine();
        forma.setCuloare(culoare);
    }
}
