package examples.inheritance;

import java.util.ArrayList;

public class Application {

    public static ArrayList<Elev> elevi = new ArrayList<>();

    public static void main(String[] args) {

        Elev ion = new Elev("Ion", 10, 10f, Gen.MASCULIN, "12345");
        Elev e1 = new Elev("Ion", 11, 10f, Gen.MASCULIN, "12346");
        Elev e2 = new Elev("Gheorghe", 12, 10f, Gen.MASCULIN, "12347");
        Elev e3 = new Elev("Vlad", 10, 10f, Gen.MASCULIN, "12348");
        Elev e4 = new Elev("Ioana", 14, 10f, Gen.FEMININ, "12349");


        elevi.add(e1);
        elevi.add(e2);
        elevi.add(e3);
        elevi.add(e4);
        elevi.add(ion);

        cautaElev();


    }

    private static void cautaElev() {
//        System.out.println("Dati numele elevului");
        // ion, 10, 10

        Elev elevCititDeLaTastatura = new Elev("Ion", 10, 10f, Gen.MASCULIN);
        for (int i = 0; i < elevi.size(); i++) {
            if (elevi.get(i) == elevCititDeLaTastatura) {
                System.out.println("L=am găsit pe elev");
            }
            if (elevi.get(i).equals(elevCititDeLaTastatura)) {
                System.out.println("L=am găsit pe elev");
            }
        }
    }
}
