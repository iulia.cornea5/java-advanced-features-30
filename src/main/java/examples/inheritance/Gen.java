package examples.inheritance;

public enum Gen {
    MASCULIN, FEMININ, NECUNOSCUT, TRANS_MASCULIN, TRANS_FEMININ
}
