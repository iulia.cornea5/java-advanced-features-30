package examples.inheritance;

public class Profesor extends Persoana {

    String materia;
    float salariul;

    public Profesor(String nume, int varsta, String materia, float salariul, Gen gen) {
        super(nume, varsta, gen);
        this.materia = materia;
        this.salariul = salariul;
    }


}
