package examples.inheritance;

public class Persoana  {
    private String nume;
    int varsta;
    Gen gen;

    public Persoana(String nume, int varsta, Gen gen) {
        this.gen = gen;
        this.nume = nume;
        this.varsta = varsta;
    }
//
//    public Persoana() {
//
//    }


    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getVarsta() {
        return varsta;
    }

    public Gen getGen() {
        return this.gen;
    }
    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }
}
