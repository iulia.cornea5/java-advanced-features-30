package examples.inheritance;


import java.util.Objects;

// clasa copil extends clasa parinte
public class Elev extends Persoana {

    float media;
    String cnp;

    public Elev(String nume, int varsta, float media, Gen gen) {
//        super();
        super(nume, varsta, gen);
        setNume(nume);
        setVarsta(varsta);


//        super(nume, varsta);
        this.varsta = varsta;
        this.media = media;
    }

    public Elev(String nume, int varsta, float media, Gen gen, String cnp) {
//        super();
        super(nume, varsta, gen);
        setNume(nume);
        setVarsta(varsta);


//        super(nume, varsta);
        this.varsta = varsta;
        this.media = media;
        this.cnp = cnp;
    }


//    public String getNume() {
//        return super.getNume();
//    }
//
//    public void setNume(String nume) {
//        super.setNume(nume);
//    }


    @Override
    public boolean equals(Object o) {
        if (o instanceof Elev &&
                this.getNume().equals(((Elev) o).getNume()) &&
                this.getVarsta() == ((Elev) o).getVarsta() &&
                this.media == ((Elev) o).media) {
            return true;
        }
        return false;
    }


}
