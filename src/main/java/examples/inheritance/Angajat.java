package examples.inheritance;

/*
Enum pentru funcția angajatului
Portar, Curațenie, Admin, Psiholog
 */


public class Angajat  extends Persoana {

    private Functie functia;
    private float salariul;

    public Angajat(String nume, Functie fucnite, int varsta, float salar, Gen gen) {
        super(nume, varsta, gen);
        this.functia = fucnite;
        this.salariul = salar;

    }

}
