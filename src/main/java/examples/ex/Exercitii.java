package examples.ex;

import examples.inheritance.*;

public class Exercitii {

    public static void main(String[] args) {

        Elev e1 = new Elev("Ion", 12, 10, Gen.MASCULIN);
        EvidentaPersoane.persoane[EvidentaPersoane.contorPersoane] = e1;
        EvidentaPersoane.contorPersoane++;


        Elev e2 = new Elev("George", 10, 9, Gen.MASCULIN);
        EvidentaPersoane.persoane[EvidentaPersoane.contorPersoane] = e2;
        EvidentaPersoane.contorPersoane++;
        Elev e3 = new Elev("Mihaela", 16, 8, Gen.FEMININ);
        EvidentaPersoane.persoane[EvidentaPersoane.contorPersoane] = e3;
        EvidentaPersoane.contorPersoane++;

        Angajat portar = new Angajat("Andreea", Functie.PORTAR, 40, 2000, Gen.NECUNOSCUT);
        EvidentaPersoane.persoane[EvidentaPersoane.contorPersoane] = portar;
        EvidentaPersoane.contorPersoane++;

        Profesor p1 = new Profesor("Doamna Miu", 34, "Matematica", 4000, Gen.FEMININ);
        EvidentaPersoane.persoane[EvidentaPersoane.contorPersoane] = p1;
        EvidentaPersoane.contorPersoane++;
        Profesor p2 = new Profesor("Domnul Boc", 30, "Romana", 4000, Gen.MASCULIN);
        EvidentaPersoane.persoane[EvidentaPersoane.contorPersoane] = p2;
        EvidentaPersoane.contorPersoane++;


        for (int i = 0; i < EvidentaPersoane.contorPersoane; i++) {
            Persoana p = EvidentaPersoane.persoane[i];
            if (p.getGen().equals(Gen.MASCULIN)) {
                System.out.print("Domnul ");
            } else if (p.getGen().equals(Gen.FEMININ)) {
                System.out.print("Doamna ");
            } else {
                System.out.print("Domnul/Doamna");
            }
            System.out.println(p.getNume());

        }


    }
}
