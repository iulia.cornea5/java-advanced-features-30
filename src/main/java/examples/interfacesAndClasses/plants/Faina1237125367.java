package examples.interfacesAndClasses.plants;

public class Faina1237125367 implements Edible{

    public void displayBrand() {
        System.out.println("Faina boromir");
    }

    public String getColor() {
        return "Neagra";
    }

    @Override
    public String getRecepies() {
        displayBrand();
        return "Paine, cozonac";
    }

    @Override
    public float getCaloriesPer100g() {
        return 500;
    }
}
