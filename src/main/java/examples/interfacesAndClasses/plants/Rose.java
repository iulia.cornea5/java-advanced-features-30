package examples.interfacesAndClasses.plants;

public class Rose extends Plant implements Edible{

    public Rose(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
    }

    @Override
    public float getSellingPrice() {
        return 0;
    }

    @Override
    public String getRecepies() {
        return "Dulceață de trandafiri";
    }

    @Override
    public float getCaloriesPer100g() {
        return 5;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Rose) {
            Rose rose = (Rose) obj;
            if(rose.getName().equals(this.getName()) &&
                    rose.getBasePrice() == this.getBasePrice() &&
                    rose.getQuantity() == this.getQuantity()) {
                return true;
            }
        }
        return false;
    }
}
