package examples.interfacesAndClasses.plants;

public class Tulip extends Plant {

    public Tulip(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
    }

    @Override
    public float getSellingPrice() {
        return 0;
    }
}
