package examples.interfacesAndClasses.plants;

public class Blueberry extends Plant implements Edible{

    public Blueberry(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
    }

    @Override
    public float getSellingPrice() {
        return 0;
    }

    @Override
    public String getRecepies() {
        return "Prăjitură cu afine, Gem de afine";
    }

    @Override
    public float getCaloriesPer100g() {
        return 40;
    }
}
