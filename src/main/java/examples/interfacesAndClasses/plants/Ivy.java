package examples.interfacesAndClasses.plants;

public class Ivy extends Plant implements CanBePoisonous{

    private boolean poisonous;
    public Ivy(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
        this.poisonous = false;
    }

    public Ivy(String name, float basePrice, int quantity, boolean poisonous) {
        super(name, basePrice, quantity);
        this.poisonous = poisonous;
    }

    @Override
    public float getSellingPrice() {
        return 0;
    }

    @Override
    public boolean isPoisonous() {
        return poisonous;
    }
}
