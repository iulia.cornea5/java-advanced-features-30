package examples.interfacesAndClasses.plants;

public interface CanBePoisonous {

    boolean isPoisonous();
}
