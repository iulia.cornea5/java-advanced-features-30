package examples.interfacesAndClasses.plants;

// interfată pentru plante ce pot fi mâncate
public interface Edible {

    // metodă să ne returneze niște rețete de mâncare pentru planta care implementează
    String getRecepies();

    float getCaloriesPer100g();

}
