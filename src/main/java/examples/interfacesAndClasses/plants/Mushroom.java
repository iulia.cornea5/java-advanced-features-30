package examples.interfacesAndClasses.plants;

import java.util.Date;

public class Mushroom extends Plant implements Edible, CanBePoisonous {
    public boolean poisonous;
    private PoisonMushroomTreatment treatment;

    public Mushroom(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
        this.poisonous = false;
    }

    public Mushroom(String name, float basePrice, int quantity, boolean poisonous) {
        super(name, basePrice, quantity);
        this.poisonous = poisonous;
    }

    public Mushroom(String name, float basePrice, int quantity, boolean poisonous, String treatmentName, TreatmentApplicationType treatmentApplication) {
        super(name, basePrice, quantity);
        this.poisonous = poisonous;
        this.treatment = new PoisonMushroomTreatment(treatmentName, treatmentApplication);
    }


    @Override
    public float getSellingPrice() {
        if (treatment != null) {
            return getBasePrice() * treatment.applicationType.priceFactor;
        } else {
            return getBasePrice();
        }
    }

    @Override
    public String getRecepies() {
        return "Tocăniță de ciuperci";
    }

    @Override
    public float getCaloriesPer100g() {
        return 100;
    }

    @Override
    public boolean isPoisonous() {
        return poisonous;
    }

    public String getTreatment() {
        if (treatment == null) {
            return "Nu exista tratament";
        } else
            return "Tratamentul pentru ciuperca se numeste " + treatment.medicineName
                    + " si se aplica " + treatment.applicationType
                    + " modul: " + treatment.applicationType.applicationStrategy;
    }

    private class PoisonMushroomTreatment {
        public String medicineName;
        public TreatmentApplicationType applicationType;
        public Date expirationDate;
        public String approval;

        public PoisonMushroomTreatment() {
        }

        public PoisonMushroomTreatment(String medicineName,
                                       TreatmentApplicationType applicationType) {
            this.medicineName = medicineName;
            this.applicationType = applicationType;
        }
    }

    public enum TreatmentApplicationType {
        ORAL(2, "Se inghite"),
        TOPIC_LOCAL(3, "Se baga in ochi"),
        TOPIC_CUTANAT(4, "Se aplica pe piele");

        private float priceFactor;
        private String applicationStrategy;

        TreatmentApplicationType(float priceFactor, String applicationStrategy) {
            this.priceFactor = priceFactor;
            this.applicationStrategy = applicationStrategy;
        }

    }
}
