package examples.interfacesAndClasses;

import examples.interfacesAndClasses.plants.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Application {

    public static void main(String[] args) {

        ArrayList<Plant> plants = new ArrayList<Plant>();
        plants.add(new Blueberry("Afin de munte", 10, 100));
        plants.add(new Ivy("Iedera de gradina", 3, 100));
        plants.add(new Rose("Trandafir rosu", 8, 50));
        plants.add(new Mushroom("Cipuerci Champignon", 10, 300));
        plants.add(new Ivy("Iedera otravitoare", 3, 10, true));
        plants.add(new Mushroom("Pălăria șarpelui", 4, 100, true));
        plants.add(new Mushroom("ciuperca otravitoare", 10, 100, true,
                "medicament antidot", Mushroom.TreatmentApplicationType.ORAL));
        plants.add(new Mushroom("ciuperca otravitoare", 10, 100));


        List<Plant> planteOtravitoare = plants.stream().filter(plant -> {
            if(plant instanceof CanBePoisonous && ((CanBePoisonous) plant).isPoisonous()) {
                return true;
            } else {
                return false;
            }
        }).collect(Collectors.toList());


        planteOtravitoare.forEach(p -> {
            float pretInitial = p.getBasePrice();
            float pretMajorat = pretInitial * 1.5f;
            p.setBasePrice(pretMajorat);
            System.out.println(p.getName() + " a avut pretul marit de la " + pretInitial + " la " + pretMajorat);
        });

















//        exampleInterfaces();
//        exampleInnerClass();
//        exampleAnonymousClass();
//        exampleEqualsOveride();

    }

    private static void exampleEqualsOveride() {
        Rose one = new Rose("trandafir", 10, 100);
//        Rose theSame = one;
        Rose theSame = new Rose("trandafir", 10, 100);

        if (one == theSame) {
            System.out.println("acelasi trandafir");
        } else {
            System.out.println("Trandafiri diferiti");
        }

        if (one.equals(theSame)) {
            System.out.println("acelasi trandafir");
        } else {
            System.out.println("Trandafiri diferiti");
        }
    }

    private static void exampleInterfaces() {

        ArrayList<Plant> plants = new ArrayList<Plant>();
        plants.add(new Blueberry("Afin de munte", 10, 100));
        plants.add(new Ivy("Iedera de gradina", 3, 100));
        plants.add(new Rose("Trandafir rosu", 8, 50));
        plants.add(new Mushroom("Cipuerci Champignon", 10, 300));
        plants.add(new Ivy("Iedera otravitoare", 3, 10, true));
        plants.add(new Mushroom("Pălăria șarpelui", 4, 100, true));


        for (int i = 0; i < plants.size(); i++) {
            Plant p = plants.get(i);
            System.out.println(p.getName());
//            if(p instanceof CanBePoisonous && ((CanBePoisonous) p).isPoisonous()) {
//                System.out.println("Este otravitoare, nu se mananca");
//            } else if(p instanceof Edible) {
//                System.out.println("Retete: " + ((Edible) p).getRecepies());
//                System.out.println("Calorii per 100g produs neprocesat: " + ((Edible) p).getCaloriesPer100g());
//            }

            if (p instanceof Edible && (p instanceof CanBePoisonous == false || (p instanceof CanBePoisonous && ((CanBePoisonous) p).isPoisonous() == false))) {
                System.out.println("Retete: " + ((Edible) p).getRecepies());
                System.out.println("Calorii per 100g produs neprocesat: " + ((Edible) p).getCaloriesPer100g());
            }
            System.out.println("-------------------------------------------------");
        }

    }

    public static void exampleInnerClass() {

        Mushroom otravioare = new Mushroom("ciuperca otravitoare", 10, 100, true,
                "medicament antidot", Mushroom.TreatmentApplicationType.ORAL);
        Mushroom neOtravioare = new Mushroom("ciuperca otravitoare", 10, 100);

        System.out.println(otravioare.getTreatment());
        System.out.println(otravioare.getSellingPrice());
        System.out.println(neOtravioare.getTreatment());
    }

    public static void exampleAnonymousClass() {

        // instantiere clasa normala - exemplu de ce se intampla aproximativ cand facem o anonymous class
        Edible fainaNeagra = new Faina1237125367();
        ((Faina1237125367) fainaNeagra).getColor();
        ((Faina1237125367) fainaNeagra).displayBrand();


        // instantiere clasa anonima - anonymous class - cu abstract class
        Plant cactus = new Plant("Cactus bunny", 10, 100) {
            @Override
            public float getSellingPrice() {
                return (getBasePrice() * 1.5f);
            }
        };

        // instantiere clasa anonima - anonymous class - cu interface
        Edible faina = new Edible() {

            public void displayBrand() {
                System.out.println("Faina boromir");
            }


            public void met12321() {
                System.out.println("wefwefsbvnwbgjhvb");
            }

            @Override
            public String getRecepies() {
                displayBrand();
                return "Paine, cozonac";
            }

            @Override
            public float getCaloriesPer100g() {
                return 500;
            }
        };

//        ((????)faina).met12321();
        System.out.println("Recepies " + faina.getRecepies());
    }
}
