package zoo;

import zoo.animal.AnimalService;
import zoo.animal.AnimalService;

import java.util.Scanner;


// [1,2,2,3,4,4,5,6,7,7]
// [1,2,3,4,5,6,7]



/* Principii OOP

Encapsularea -
Abtractizarea -
Moștenirea -
Polimorfismul -

*/

/*

Temă:
Pentru Employee și EmployeeService implementăm analog cu Animal și AnimalService


Employee (cel puțin:    String name;     int age;     float salary;)
- contrctori
- getters
- setters
- toString

EmployeeService
- localEmployees
- localContor
- employees
- contor
- createEmployee
- getUserToCreateEmployee
- editEmployee

zoo.ZooController
- cod pentru a adăuga angajați în 3 locații
- cod pentru a edita un angajat

AnimalTest
- toStringTest

EmplyeeTest
- toStringTest

EmployeeServiceTest
- createEmployeeTest


 */

public class ZooController {
    // s1 0011011101
    // s2 0011011101

    static String testăm(String s1, String s2) {
        return s1 + s2;
    }


    public static void main(String[] args) {

        //tipul_obiectului nume_obiect = new constructor
        AnimalService animalServiceTgMures = new AnimalService("Tg Mures");
        AnimalService animalServiceOradea = new AnimalService("Oradea");
        AnimalService animalServiceBucuresti = new AnimalService("Bucuresti");

        System.out.println("Animale din bucuresti: ");
        animalServiceBucuresti.getUserToCreateAnimal();
        animalServiceBucuresti.getUserToCreateAnimal();
        System.out.println("Animale din Oradea: ");
        animalServiceOradea.getUserToCreateAnimal();
        animalServiceOradea.getUserToCreateAnimal();

        animalServiceBucuresti.displayLocalAnimals();
        animalServiceBucuresti.displayAllAnimals();

        animalServiceOradea.displayLocalAnimals();
        animalServiceOradea.displayAllAnimals();


        System.out.println("----------------------------------------");
        System.out.println("Selectați locația: ");
        System.out.println("1. București");
        System.out.println("2. Oradea");
        System.out.println("3. Tg Mureș");
        Scanner keyboard = new Scanner(System.in);
        int optiuneOras = keyboard.nextInt();

        switch (optiuneOras) {
            case 1:
                animalServiceBucuresti.editAnimal();
                animalServiceBucuresti.displayLocalAnimals();
                break;
            case 2:
                animalServiceOradea.editAnimal();
                animalServiceOradea.displayLocalAnimals();
                break;
            case 3:
                animalServiceTgMures.editAnimal();
                animalServiceTgMures.displayLocalAnimals();
                break;
        }

        animalServiceOradea.displayAllAnimals();

    }

}
